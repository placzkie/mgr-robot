from collections import Counter

from gaze.NaoGaze import NaoGaze
from utils.Constants import GazeDirection
from utils.Utils import *





class Analyser(object):

    def __init__(self):
        self.gaze = NaoGaze("test")
        self.results = []


    def setup(self):
        self.gaze.subscribe()
        pass

    def analyse(self):
        gazeDirection = self.gaze.getGazeDirection()
        headAngles = self.gaze.getHeadAngles()
        res = self.__performAnalysis(gazeDirection, headAngles)
        self.results.append(res)

        pass

    def getResults(self):
        result = self.__performStatisticAnalisis()
        self.results = []
        return result

    #  1|2|3
    #  4|5|6
    #  7|8|9
    def __performStatisticAnalisis(self):
        counter = Counter()
        for res in self.results:
            counter[res[0]] += 1

        return counter.most_common(1)[0][0]





    def __performAnalysis(self, gazeDirection, headAngles):
        result = ()
        direction = (gazeDirection[0] + headAngles[0], gazeDirection[1] + headAngles[1], headAngles[2])
        yaw, pitch, roll = transformToDeg(direction)
        if yaw in range(-5, 5):
            if pitch in range(-5, 5):
                result = (GazeDirection.LOOKING_ON_ROBOT_FACE, (yaw, pitch, roll))

        if yaw in range(-90, -20):
            if pitch in range(-90, -20):
                result = (GazeDirection.LOOKING_ON_LEFT_DOWN, (yaw, pitch, roll))
        if yaw in range(20, 90):
            if pitch in range(-90, -20):
                result = (GazeDirection.LOOKING_ON_RIGHT_DOWN, (yaw, pitch, roll))
        if yaw in range(-20, 20):
            if pitch in range(-90, -20):
                result = (GazeDirection.LOOKING_ON_MIDDLE_DOWN, (yaw, pitch, roll))

        if yaw in range(-90, -20):
            if pitch in range(-20, 20):
                result = (GazeDirection.LOOKING_ON_LEFT_MIDDLE, (yaw, pitch, roll))
        if yaw in range(20, 90):
            if pitch in range(-20, 20):
                result = (GazeDirection.LOOKING_ON_RIGHT_MIDDLE, (yaw, pitch, roll))
        if yaw in range(-20, 20):
            if pitch in range(-20, 20):
                result = (GazeDirection.LOOKING_ON_MIDDLE_MIDDLE, (yaw, pitch, roll))

        if yaw in range(-90, -20):
            if pitch in range(20, 90):
                result = (GazeDirection.LOOKING_ON_LEFT_UP, (yaw, pitch, roll))
        if yaw in range(20, 90):
            if pitch in range(20, 90):
                result = (GazeDirection.LOOKING_ON_RIGHT_UP, (yaw, pitch, roll))
        if yaw in range(-20, 20):
            if pitch in range(20, 90):
                result = (GazeDirection.LOOKING_ON_MIDDLE_UP, (yaw, pitch, roll))

        # TODO maybe something better
        return result
