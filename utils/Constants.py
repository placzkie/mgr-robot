from enum import Enum


class GazeDirection(Enum):
    LOOKING_ON_ROBOT_FACE = "looking_on_robot_face"
    LOOKING_ON_LEFT_DOWN = "looking_on_left_down"
    LOOKING_ON_RIGHT_DOWN = "looking_on_right_down"
    LOOKING_ON_MIDDLE_DOWN = "looking_on_middle_down"
    LOOKING_ON_LEFT_MIDDLE = "looking_on_left_middle"
    LOOKING_ON_RIGHT_MIDDLE = "looking_on_right_middle"
    LOOKING_ON_MIDDLE_MIDDLE = "looking_on_middle_middle"
    LOOKING_ON_LEFT_UP = "looking_on_left_up"
    LOOKING_ON_RIGHT_UP = "looking_on_right_up"
    LOOKING_ON_MIDDLE_UP = "looking_on_middle_up"

