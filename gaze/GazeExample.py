import cv2
from NaoVideoSource import NaoVideoSourceRemote
from gaze.WebcamVideoSource import WebcamVideoSource


def main_webcam():
    cv2.namedWindow('Video')
    cv2.moveWindow('Video', 20, 20)

    vs = WebcamVideoSource()
    vs.subscribeCamera()

    while True:
        frame = vs.getImage()
        vs.releaseImage()

        cv2.imshow('Video', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    vs.unsubscribe()


def main_nao():
    cv2.namedWindow('Video')
    cv2.moveWindow('Video', 20, 20)

    vs = NaoVideoSourceRemote(ip='127.0.0.1')
    # vs = NaoVideoSourceRemote(ip='169.254.113.113')
    vs.subscribeCamera()

    while True:
        img = vs.getImage()
        vs.releaseImage()

        img = cv2.cvtColor(img, cv2.COLOR_YUV2RGB)

        cv2.imshow('Video', img)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    vs.unsubscribe()

if __name__ == "__main__":
    main_nao()
