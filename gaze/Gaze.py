
class Gaze(object):

    def subscribe(self):
        raise NotImplementedError("Should have implemented this")

    def unsubscribe(self):
        raise NotImplementedError("Should have implemented this")

    def getGazeDirection(self, id):
        raise NotImplementedError("Should have implemented this")

    def getHeadAngles(self, id):
        raise NotImplementedError("Should have implemented this")

    def isLookingAtRobot(self, id):
        raise NotImplementedError("Should have implemented this")

    def lookingAtRobotScore(self, id):
        raise NotImplementedError("Should have implemented this")
