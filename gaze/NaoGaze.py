import time

import sys
from optparse import OptionParser

from gaze.Gaze import Gaze
from naoqi import ALBroker, ALModule, ALProxy

NAO_IP = "nao.local"

NaoGazeInst = None
memory = None


class NaoGaze(ALModule, Gaze):

    def __init__(self, name):
        ALModule.__init__(self, name)
        self.name = name
        self.subscriber_name = "Nao_Gaze"
        self.ga_proxy = ALProxy("ALGazeAnalysis")
        self.id = None

        global memory
        memory = ALProxy("ALMemory")
        memory.subscribeToEvent("GazeAnalysis/PersonStartsLookingAtRobot", self.name, "onPersonStartsLookingAtRobot")
        memory.subscribeToEvent("GazeAnalysis/PersonStopsLookingAtRobot", self.name, "onPersonStopsLookingAtRobot")
        memory.subscribeToEvent("GazeAnalysis/PeopleLookingAtRobot", self.name, "onPeopleLookingAtRobot")

    def onPersonStartsLookingAtRobot(self, eventName, id, subscriberIdentifier):
        """ This will be called each time a person starts looking at robot."""

        self.id = id
        print('PersonStartsLookingAtRobot occured: %s ' % id)

    def onPersonStopsLookingAtRobot(self, eventName, id, subscriberIdentifier):
        """ This will be called each time a person stops looking at robot."""

        self.id = None
        print('PersonStopsLookingAtRobot occured %s' % id)

    def onPeopleLookingAtRobot(self, eventName, idList, subscriberIdentifier):
        """ This will be called each time people looking at robot changes."""

        print('PeopleLookingAtRobot occured %s' % idList)
        # TODO - should we handle this somehow or assume only one person interacts with a robot?

    def subscribe(self):
        period = 500
        self.ga_proxy.subscribe(self.subscriber_name, period, 0.1)

    def unsubscribe(self):
        self.ga_proxy.unsubscribe(self.subscriber_name)

    def getHeadAngles(self):
        val = None
        if self.id is not None:
            # (yaw, pitch, roll) in radians
            val = memory.getData("PeoplePerception/Person/%s/HeadAngles" % self.id)
        return val

    def getGazeDirection(self):
        val = None
        if self.id is not None:
            # [yaw, pitch] in radians
            val = memory.getData("PeoplePerception/Person/%s/GazeDirection" % self.id)
        return val

    def isLookingAtRobot(self):
        val = memory.getData("PeoplePerception/Person/%s/IsLookingAtRobot" % self.id)
        return val

    def lookingAtRobotScore(self):
        val = memory.getData("PeoplePerception/Person/%s/LookingAtRobotScore" % self.id)
        return val


def main():
    """ Main entry point"""
    parser = OptionParser()
    parser.add_option("--pip",
                      help="Parent broker port. The IP address of your robot",
                      dest="pip")
    parser.add_option("--pport",
                      help="Parent broker port. The port NAOqi is listening to",
                      dest="pport",
                      type="int")
    parser.set_defaults(
        pip=NAO_IP,
        pport=9559)

    (opts, args_) = parser.parse_args()
    pip = opts.pip
    pport = opts.pport
    # We need this broker to be able to construct
    # NAOqi modules and subscribe to other modules
    # The broker must stay alive until the program exists
    broker = ALBroker("NaoGazeBroker",
                        "0.0.0.0",  # listen to anyone
                        0,  # find a free port and use it
                        pip,  # parent broker IP
                        pport)  # parent broker port

    # Warning: HumanGreeter must be a global variable
    # The name given to the constructor must be the name of the
    # variable
    global NaoGazeInst
    NaoGazeInst = NaoGaze("NaoGazeInst")
    NaoGazeInst.subscribe()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print
        print "Interrupted by user, shutting down"
        NaoGazeInst.unsubscribe()
        broker.shutdown()
        sys.exit(0)


if __name__ == "__main__":
    main()

