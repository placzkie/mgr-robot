import numpy

from VideoSource import VideoSource
from naoqi import ALProxy
import vision_definitions


class NaoVideoSourceRemote(VideoSource):

    def __init__(self, ip='nao.local', port=9559):
        self.vd_proxy = ALProxy("ALVideoDevice", ip, port)
        self.resolution = vision_definitions.kQVGA
        self.colorSpace = vision_definitions.kYUVColorSpace
        self.fps = 20
        self.nameId = None

    def subscribeCamera(self):
        #subscribe or subscribeCamera?? - subscribe is deprecated
        # self.nameId = self.vd_proxy.subscribeCamera("python_GVM", 0, self.resolution, self.colorSpace, self.fps)
        self.nameId = self.vd_proxy.subscribe("python_GVM", self.resolution, self.colorSpace, self.fps)

    def getImage(self):
        image = self.vd_proxy.getImageRemote(self.nameId)
        image_width = image[0]
        image_height = image[1]

        return numpy.fromstring(image[6], numpy.uint8).reshape(image_height, image_width, 3)

    def releaseImage(self):
        self.vd_proxy.releaseImage(self.nameId)

    def unsubscribe(self):
        self.vd_proxy.unsubscribe(self.nameId)


class NaoVideoSourceLocal(VideoSource):
    def __init__(self, ip, port=9559):
        self.vd_proxy = ALProxy("ALVideoDevice", ip, port)
        self.resolution = vision_definitions.kQVGA
        self.colorSpace = vision_definitions.kYUVColorSpace
        self.fps = 20
        self.nameId = None

    def subscribeCamera(self):
        # TODO - subscribeCamera
        self.nameId = self.vd_proxy.subscribe("python_GVM", self.resolution, self.colorSpace, self.fps)

    def getImage(self):
        #returns AL::ALImage*
        image = self.vd_proxy.getImageLocal(self.nameId)

        #TODO - extract data from image
        return image

    def releaseImage(self):
        self.vd_proxy.releaseImage(self.nameId)

    def unsubscribe(self):
        self.vd_proxy.unsubscribe(self.nameId)
