from gaze.Gaze import Gaze
from gaze.GazeEstimator import GazeEstimator
from gaze.HeadEstimator import HeadEstimator
from gaze.WebcamVideoSource import WebcamVideoSource


class CustomGaze(Gaze):
    def __init__(self):
        self.gazeData = ()
        self.headAngles = ()
        self.gazeEstimator = GazeEstimator()
        self.headEstimator = HeadEstimator()
        self.videoSource = WebcamVideoSource()

    def subscribe(self):
        self.videoSource.subscribeCamera()

    def lookingAtRobotScore(self, id):
        self.videoSource.unsubscribe()

    def getGazeDirection(self, id):
        frame = self.videoSource.getImage()
        self.gazeData = self.gazeEstimator.estimate(frame)
        return self.gazeData

    def isLookingAtRobot(self, id):
        pass

    def getHeadAngles(self, id):
        frame = self.videoSource.getImage()
        self.headAngles = self.headEstimator.estimate(frame)
        return self.headAngles


    def unsubscribe(self):
        self.videoSource.unsubscribe()
