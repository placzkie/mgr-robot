import cv2
from naoqi import ALProxy


def main():
    vd_proxy = ALProxy("ALVideoDevice", '127.0.0.1', 9559)

    vc = cv2.VideoCapture(0)
    if not vc.isOpened():
        print("Error: Failed to open camera")
    else:
        print("The video source has been opened correctly.")

    while True:
        ret, frame = vc.read()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        res = vd_proxy.putImage(0, frame.shape[1], frame.shape[0], frame.tobytes())
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    vc.release()


if __name__ == "__main__":
    main()
