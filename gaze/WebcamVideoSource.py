import cv2
from VideoSource import VideoSource


class WebcamVideoSource(VideoSource):

    def __init__(self):
        self.video_capture = None
        self.cam_width = None
        self.cam_height = None

    def subscribeCamera(self):
        self.video_capture = cv2.VideoCapture(0)
        if not self.video_capture.isOpened():
            print("Error: Failed to open camera")
        else:
            print("The video source has been opened correctly.")

        self.cam_width = int(self.video_capture.get(3))
        self.cam_height = int(self.video_capture.get(4))

    def getImage(self):
        ret, frame = self.video_capture.read()
        return frame

    def releaseImage(self):
        pass

    def unsubscribe(self):
        self.video_capture.release()
