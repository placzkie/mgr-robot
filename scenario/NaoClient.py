from naoqi import ALProxy

NAO_IP = "nao.local"


class NaoClient(object):
    def __init__(self):
        self.tts = ALProxy("ALTextToSpeech", NAO_IP, 9559)
        self.motion = ALProxy("ALMotion", NAO_IP, 9559)

    def say(self, text):
        self.tts.say(text)

    def moveHead(self):
        #TODO - implement
        pass

    #TODO - add other actions