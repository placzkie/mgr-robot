import time

import math

from analyse.Analyser import Analyser
from scenario.JobInterviewScenario import JobInterviewScenario
from scenario.NaoClient import NaoClient

class ScenarioControler(object):
    def __init__(self):
        self.period = 0.5
        self.anlyser = Analyser()
        self.scenario = JobInterviewScenario()
        self.naoClient = NaoClient()

    def react(self, action):
        action.run(self.naoClient)

    def run(self):
        numOfStages = self.scenario.getNumberOfStages()
        action, waitTime = self.scenario.getGreeting()
        self.react(action)
        time.sleep(waitTime)

        for i in range(numOfStages):
            action, waitTime, stopAction, responseCases = self.scenario.getStage(i)
            self.react(action)
            probes = math.floor(waitTime / self.period)
            nexttime = time.time() + self.period
            for i in range(probes):
                self.anlyser.analyse()
                now = time.time()
                tosleep = nexttime - now
                if tosleep > 0:
                    time.sleep(tosleep)
                    nexttime += self.period
                else:
                    nexttime = now + self.period
            result = self.anlyser.getResults()
            self.react(stopAction)
            if responseCases.has_key(result):
                self.react(responseCases[result])
