
class Action(object):
    def run(self, nao_client):
        raise NotImplementedError("Should have implemented this")

    @staticmethod
    def of(actionJson):
        if actionJson is None:
            return EmptyAction()
        elif actionJson['type'] == 'say':
            return SayAction(actionJson['text'])
        elif actionJson['type'] == 'moveHead':
            return MoveHeadAction()


class EmptyAction(Action):
    def run(self, nao_client):
        pass


class SayAction(Action):

    def __init__(self, text):
        self.text = text

    def run(self, nao_client):
        nao_client.say(self.text)


class MoveHeadAction(Action):

    def run(self, nao_client):
        nao_client.moveHead()
