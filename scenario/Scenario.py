import json

from scenario.Action import Action
from utils.Constants import GazeDirection


class Scenario(object):
    def getNumberOfStages(self):
        raise NotImplementedError("Should have implemented this")

    def getGreeting(self):
        raise NotImplementedError("Should have implemented this")

    def getStage(self, i):
        raise NotImplementedError("Should have implemented this")


def loadScenario(path):
    config = json.load(open(path))
    greeting = Action.of(config['greeting'])
    stages = __parseStages(config['stages'])
    return greeting, stages


def __parseStages(stagesJson):
    stages = []
    for stage in stagesJson:
        startAction = Action.of(stage['startAction'])
        waitTime = stage['waitTime']
        stopAction = Action.of(stage['stopAction'])
        responseCases = {}
        for key, val in stage['responseCases'].items():
            action = Action.of(val)
            if key != 'default':
                responseCases[GazeDirection(key)] = action
        if 'default' in stage['responseCases']:
            for gaze_direction in GazeDirection:
                if gaze_direction not in responseCases:
                    responseCases[gaze_direction] = Action.of(stage['responseCases']['default'])

        stages.append((startAction, waitTime, stopAction, responseCases))

    return stages