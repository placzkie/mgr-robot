from scenario.Scenario import Scenario, loadScenario


class JobInterviewScenario(Scenario):

    def __init__(self):
        self.greeting, self.stages = loadScenario('job_interview_scenario.json')

    def getStage(self, i):
        stage = None
        if i < len(self.stages):
            stage = self.stages[i]
        return stage

    def getNumberOfStages(self):
        return len(self.stages)

    def getGreeting(self):
        return self.greeting
